.. _exceptions:

Exceptions
==========

Exceptions implemented by sdh.

.. tip::

    For a list of error codes that are visible to the user see :ref:`exit codes <exit_codes>`.

.. currentmodule:: sdh.exceptions

.. autoexception:: SDHException
.. autoexception:: UserError

.. _exit_codes:

Exit Codes
==========

The application has the following possible exit codes:

.. code-block:: none

     0  success
     1  user error
     2  evaluated warning
     3  evaluated error
     4  evaluated critical

.. hint::

    For information on exceptions used internally associated with these exit codes see :ref:`exceptions <exceptions>`.

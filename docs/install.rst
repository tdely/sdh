.. _install:

Installation
============

There are different ways to install sdh:

- :ref:`source-install`, either:

  - :ref:`pip-installation`
  - :ref:`git-installation`


.. _source-install:

From Source
-----------

Dependencies
~~~~~~~~~~~~

* `Python 3`_ >= 3.6.0.

.. _Python 3: https://www.python.org/

.. _pip-installation:

Using pip
~~~~~~~~~

It's recommended to use a `virtual environment`_ to avoid dependency conflicts with other Python packages or issues with
permissions and ownership.

.. _virtual environment: https://docs.python.org/3/library/venv.html

.. note::

    If installing into a virtual environment you need to activate the environment (source venv/bin/activate) for each
    new session.

This will use :code:`pip` to install the latest release:

.. code-block:: console

    python -m pip install git+https://gitlab.com/tdely/sdh@master

.. _git-installation:

Using git
~~~~~~~~~

This uses latest release code from git.

.. code:: console

    git clone https://gitlab.com/tdely/sdh
    cd sdh

    # create a virtual environment
    python -m pip venv venv
    source venv/bin/activate

    python -m pip install -e ".[dev]"  # in-place editable mode with development dependencies

.. _quickstart:
.. highlight:: console

Quickstart
==========

.. note::

    Make sure that:

    * sdh is :ref:`installed <install>` and up-to-date.
    * Your :ref:`configuration file <config>` has the correct settings.

Checking the health of drive `sda`:

.. code-block::

    sdh sda

Checking the health of drives `sda`, `sdb`, and the ZFS pool `tank`:

.. code-block::

    sdh sda sdb zfs tank

The output can be sent by email instead of printed to stdout,
using `\--email` requires the inclusion of a configuration file:

.. code-block::

    sdh --cfg config.yml --email sda

Output can be suppressed if health checks pass through the `\--quiet` option:

.. code-block::

    sdh --quiet sda

The `\--quiet` option is most useful when combined with `\--email`:

.. code-block::

    sdh --cfg config.yml --email --quiet sda

.. include:: ../README.rst

Release v\ |version|.

The sections below describe the use of the application:

.. toctree::
   :maxdepth: 1

   quickstart

For information on how to setup the application or on changes between versions,
please see the sections below:

.. toctree::
   :maxdepth: 1

   install
   config
   exit_codes
   changes

Documentation on the application internals:

.. toctree::
   :maxdepth: 1

   developer
   sdh_tools
   exceptions

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
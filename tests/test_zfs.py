# -*- coding: utf-8 -*-
from unittest import mock

import pytest

from sdh import exceptions
from sdh.constants import State
from sdh.zfs import ZFS


class TestZFS(object):
    def test_format_output_mandatory_only(self):
        pool = mock.MagicMock()
        pool.name = "test1"
        pool.status = None
        pool.action = None
        pool.see = None
        output = ZFS._format_output(pool)
        assert "ZFS pool test1" in output
        assert "Status:" not in output
        assert "Action:" not in output
        assert "See:" not in output

    def test_format_output_conditionals(self):
        pool = mock.MagicMock()
        pool.status = "status string"
        pool.action = "action string"
        pool.see = "see string"
        output = ZFS._format_output(pool)
        assert "Status:" in output
        assert "Action:" in output
        assert "See:" in output

    def test_check_pool_zpool_missing(self):
        with mock.patch("sdh.zfs.Pool") as Pool:
            Pool.side_effect = RuntimeError("test")
            with pytest.raises(exceptions.UserError):
                ZFS({}).check_pool("test1")

    def test_check_pool_ok(self):
        with mock.patch("sdh.zfs.Pool") as Pool:
            pool = mock.MagicMock()
            pool.errors = ["No known data errors"]
            pool.state = "OK"
            Pool.return_value = pool
            state, output = ZFS({}).check_pool("test1")
            assert state == State.OK

    def test_check_pool_warn(self):
        with mock.patch("sdh.zfs.Pool") as Pool:
            pool = mock.MagicMock()
            pool.errors = [
                "Permanent errors have been detected in the following files:",
                "<metadata>:<0x0>",
            ]
            pool.state = "OK"
            Pool.return_value = pool
            state, output = ZFS({}).check_pool("test1")
            assert state == State.WARNING

    def test_check_pool_error(self):
        with mock.patch("sdh.zfs.Pool") as Pool:
            pool = mock.MagicMock()
            pool.errors = [
                "Permanent errors have been detected in the following files:",
                "<metadata>:<0x0>",
            ]
            pool.state = "DEGRADED"
            Pool.return_value = pool
            state, output = ZFS({}).check_pool("test1")
            assert state == State.ERROR

    def test_check_pool_crit_faulted(self):
        with mock.patch("sdh.zfs.Pool") as Pool:
            pool = mock.MagicMock()
            pool.errors = [
                "Permanent errors have been detected in the following files:",
                "<metadata>:<0x0>",
            ]
            pool.state = "FAULTED"
            Pool.return_value = pool
            state, output = ZFS({}).check_pool("test1")
            assert state == State.CRITICAL

    def test_check_pool_crit_unavail(self):
        with mock.patch("sdh.zfs.Pool") as Pool:
            pool = mock.MagicMock()
            pool.errors = [
                "Permanent errors have been detected in the following files:",
                "<metadata>:<0x0>",
            ]
            pool.state = "UNAVAIL"
            Pool.return_value = pool
            state, output = ZFS({}).check_pool("test1")
            assert state == State.CRITICAL

    def test_check_pools(self):
        zfs = ZFS({})
        with mock.patch("sdh.zfs.ZFS.check_pool") as check_pool:
            check_pool.side_effect = [
                (State.OK, "output"),
            ]
            state, output = zfs.check_pools(["test1"])
            assert state == State.OK
            assert "output" in output

    def test_check_pools_supersede_state(self):
        zfs = ZFS({})
        with mock.patch("sdh.zfs.ZFS.check_pool") as check_pool:
            check_pool.side_effect = [
                (State.OK, "output1"),
                (State.ERROR, "output2"),
                (State.OK, "output3"),
            ]
            state, output = zfs.check_pools(["test1", "test2", "test3"])
            assert state == State.ERROR
            assert "output1" in output
            assert "output3" in output

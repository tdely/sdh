# -*- coding: utf-8 -*-
from unittest import mock

import pytest

from sdh import exceptions
from sdh.constants import State
from sdh.smart import SMART


@pytest.fixture()
def setup():
    with mock.patch("sdh.smart.which") as which:
        which.return_value = "/usr/sbin/smartctl"
        yield


class TestSMART(object):
    def test_smartctl_missing(self):
        with mock.patch("sdh.smart.which") as which:
            which.return_value = None
            with pytest.raises(exceptions.UserError) as e:
                SMART({})
        assert str(e.value).startswith("smartctl command")

    def test_schema_empty(self, setup):
        assert SMART({})

    def test_temp_check_unknown(self, setup):
        smart = SMART({})
        dev = mock.MagicMock()
        dev.temperature = None
        assert smart._temp_check(dev) == (State.OK, "unknown")

    def test_temp_check_ok(self, setup):
        smart = SMART({})
        dev = mock.MagicMock()
        dev.temperature = 20
        assert smart._temp_check(dev) == (State.OK, "OK")

    def test_temp_check_low_warn(self, setup):
        smart = SMART({})
        dev = mock.MagicMock()
        dev.temperature = smart._default["warn_low"]
        assert smart._temp_check(dev) == (State.WARNING, "low")

    def test_temp_check_low_critical(self, setup):
        smart = SMART({})
        dev = mock.MagicMock()
        dev.temperature = smart._default["critical_low"]
        assert smart._temp_check(dev) == (State.CRITICAL, "low")

    def test_temp_check_high_warn(self, setup):
        smart = SMART({})
        dev = mock.MagicMock()
        dev.temperature = smart._default["warn_high"]
        assert smart._temp_check(dev) == (State.WARNING, "high")

    def test_temp_check_high_critical(self, setup):
        smart = SMART({})
        dev = mock.MagicMock()
        dev.temperature = smart._default["critical_high"]
        assert smart._temp_check(dev) == (State.CRITICAL, "high")

    def test_check_disk_missing_disk(self, setup):
        smart = SMART({})
        with mock.patch("sdh.smart.Device") as Device:
            Device.side_effect = UserWarning("blah")
            with pytest.raises(exceptions.UserError):
                smart.check_disk("test")

    def test_check_disk_pass(self, setup):
        smart = SMART({})
        with mock.patch("sdh.smart.Device") as Device:
            dev = mock.MagicMock()
            dev.name = "test"
            dev.temperature = 20
            dev.assessment = "PASS"
            dev.model = "MODEL"
            dev.serial = "SERIAL"
            dev.messages = []
            Device.return_value = dev
            state, output = smart.check_disk("test")
            assert state == State.OK

    def test_check_disk_warn(self, setup):
        smart = SMART({})
        with mock.patch("sdh.smart.Device") as Device:
            dev = mock.MagicMock()
            dev.name = "test"
            dev.temperature = 20
            dev.assessment = "WARN"
            dev.model = "MODEL"
            dev.serial = "SERIAL"
            dev.messages = []
            Device.return_value = dev
            state, output = smart.check_disk("test")
            assert state == State.ERROR

    def test_check_disk_fail(self, setup):
        smart = SMART({})
        with mock.patch("sdh.smart.Device") as Device:
            dev = mock.MagicMock()
            dev.name = "test"
            dev.temperature = 20
            dev.assessment = "FAIL"
            dev.model = "MODEL"
            dev.serial = "SERIAL"
            dev.messages = []
            Device.return_value = dev
            state, output = smart.check_disk("test")
            assert state == State.ERROR

    def test_check_disk_output(self, setup):
        smart = SMART({})
        with mock.patch("sdh.smart.Device") as Device:
            dev = mock.MagicMock()
            dev.name = "test"
            dev.temperature = 20
            dev.assessment = "FAIL"
            dev.model = "MODEL"
            dev.serial = "SERIAL"
            dev.messages = []
            Device.return_value = dev
            state, output = smart.check_disk("test")
            assert "Drive test" in output
            assert "MODEL" in output
            assert "SERIAL" in output
            assert "20" in output
            assert "FAIL" in output

    def test_check_disk_output_messages(self, setup):
        smart = SMART({})
        with mock.patch("sdh.smart.Device") as Device:
            dev = mock.MagicMock()
            dev.name = "test"
            dev.temperature = 20
            dev.assessment = "FAIL"
            dev.model = "MODEL"
            dev.serial = "SERIAL"
            dev.messages = ["SMART message 1", "SMART message 2"]
            Device.return_value = dev
            state, output = smart.check_disk("test")
            assert "SMART message 1" in output
            assert "SMART message 2" in output

    def test_check_disks(self, setup):
        smart = SMART({})
        with mock.patch("sdh.smart.SMART.check_disk") as check_disk:
            check_disk.side_effect = [(State.OK, "output")]
            state, output = smart.check_disks(["test1"])
            assert state == State.OK
            assert "output" in output

    def test_check_disks_supersede_state(self, setup):
        smart = SMART({})
        with mock.patch("sdh.smart.SMART.check_disk") as check_disk:
            check_disk.side_effect = [
                (State.OK, "output1"),
                (State.ERROR, "output2"),
                (State.OK, "output3"),
            ]
            state, output = smart.check_disks(["test1", "test2", "test3"])
            assert state == State.ERROR
            assert "output1" in output
            assert "output3" in output

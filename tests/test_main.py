# -*- coding: utf-8 -*-
import tempfile
from unittest import mock

import pytest
from docopt import DocoptExit

from sdh import constants, exceptions, main


class TestLoadArgs(object):
    def test_dev_only(self):
        args = main.load_args(["sda"])
        assert args.get("<dev>")
        assert not args.get("zfs")

    def test_dev_length(self):
        assert len(main.load_args(["sda"]).get("<dev>")) == 1
        assert len(main.load_args(["sda", "sdb"]).get("<dev>")) == 2

    def test_zfs_only(self):
        args = main.load_args(["zfs", "test1"])
        assert not args.get("<dev>")
        assert args.get("zfs")
        assert args.get("<zpool>")

    def test_zfs_length(self):
        args = main.load_args(["zfs", "test1"])
        assert not args.get("<dev>")
        assert args.get("zfs")
        assert len(args.get("<zpool>")) == 1
        assert len(main.load_args(["zfs", "test1", "test2"]).get("<zpool>")) == 2

    def test_zfs_zpool_missing(self):
        with pytest.raises(DocoptExit):
            main.load_args(["zfs"])

    def test_dev_zfs(self):
        args = main.load_args(["sda", "zfs", "test1"])
        assert args.get("zfs")
        assert args.get("<zpool>")
        assert args.get("<dev>")

    def test_empty(self):
        with pytest.raises(DocoptExit):
            main.load_args([])

    def test_schema_cfg(self):
        with pytest.raises(exceptions.UserError):
            main.load_args(["--cfg", "", "sda"])


class TestLoadConfig(object):
    def test_no_cfg(self):
        cfg = main.load_config("")
        assert cfg.get("smart") == {}
        assert not cfg.get("email")

    def test_empty_cfg(self):
        with tempfile.NamedTemporaryFile("w") as f:
            cfg = main.load_config("")
            assert isinstance(cfg, dict)

    def test_smart_exist(self):
        with tempfile.NamedTemporaryFile("w") as f:
            f.write("smart:\n  key: 'value'")
            f.flush()
            cfg = main.load_config(f.name)
            assert cfg.get("smart")

    def test_smart_invalid_type(self):
        with tempfile.NamedTemporaryFile("w") as f:
            f.write("smart: 0")
            f.flush()
            with pytest.raises(exceptions.UserError) as e:
                main.load_config(f.name)
            assert "smart must be" in str(e.value)


class TestMain(object):
    def test_main(self):
        with mock.patch("sdh.main.SMART") as SMART:
            smart = mock.MagicMock()
            smart.check_disks.return_value = constants.State.OK, "smart"
            SMART.return_value = smart
            with mock.patch("sdh.main.ZFS") as ZFS:
                zfs = mock.MagicMock()
                zfs.check_pools.return_value = constants.State.OK, "zpool"
                ZFS.return_value = zfs
                exit_code = main.main(["sda"])
                smart.check_disks.assert_called_once_with(["sda"])
                zfs.check_pools.assert_not_called()
                assert exit_code == constants.E_SUCCESS

    def test_main_zfs(self):
        with mock.patch("sdh.main.SMART") as SMART:
            smart = mock.MagicMock()
            smart.check_disks.return_value = constants.State.OK, ["smart"]
            SMART.return_value = smart
            with mock.patch("sdh.main.ZFS") as ZFS:
                zfs = mock.MagicMock()
                zfs.check_pools.return_value = constants.State.OK, ["zpool"]
                ZFS.return_value = zfs
                exit_code = main.main(["sda", "zfs", "test1"])
                smart.check_disks.assert_called_once_with(["sda"])
                zfs.check_pools.assert_called_once_with(["test1"])
                assert exit_code == constants.E_SUCCESS

    def test_main_zfs_supersede(self):
        with mock.patch("sdh.main.SMART") as SMART:
            smart = mock.MagicMock()
            smart.check_disks.return_value = constants.State.OK, ["smart"]
            SMART.return_value = smart
            with mock.patch("sdh.main.ZFS") as ZFS:
                zfs = mock.MagicMock()
                zfs.check_pools.return_value = constants.State.ERROR, ["zpool"]
                ZFS.return_value = zfs
                exit_code = main.main(["sda", "zfs", "test1"])
                smart.check_disks.assert_called_once_with(["sda"])
                zfs.check_pools.assert_called_once_with(["test1"])
                assert exit_code == constants.E_SDH_ERROR

    def test_main_user_error(self):
        with mock.patch("sdh.main.SMART") as SMART:
            smart = mock.MagicMock()
            smart.check_disks.side_effect = exceptions.UserError("test")
            SMART.return_value = smart
            exit_code = main.main(["sda"])
            assert exit_code == constants.E_USER_ERR

    def test_main_email(self):
        with mock.patch("sdh.main.load_config") as load_config:
            load_config.return_value = {
                "email": {"test": "test"},
                "smart": {},
            }
            with mock.patch("sdh.main.SMART") as SMART:
                smart = mock.MagicMock()
                smart.check_disks.return_value = constants.State.OK, ["smart"]
                SMART.return_value = smart
                with mock.patch("sdh.main.EmailSender") as email_sender:
                    email = mock.MagicMock()
                    email.send.return_value = constants.E_SUCCESS
                    email_sender.return_value = email
                    exit_code = main.main(["--email", "sda"])
                    load_config.assert_called_once()
                    email.send.assert_called_once()
                    assert exit_code == constants.E_SUCCESS

    def test_main_email_no_cfg(self):
        with mock.patch("sdh.main.SMART") as SMART:
            smart = mock.MagicMock()
            smart.check_disks.return_value = constants.State.OK, ["smart"]
            SMART.return_value = smart
            with mock.patch("sdh.main.EmailSender") as email:
                email.send.return_value = constants.E_SUCCESS
                exit_code = main.main(["--email", "sda"])
                email.send.assert_not_called()
                assert exit_code == constants.E_USER_ERR

    def test_main_email_quiet_ok(self):
        with mock.patch("sdh.main.SMART") as SMART:
            smart = mock.MagicMock()
            smart.check_disks.return_value = constants.State.OK, ["smart"]
            SMART.return_value = smart
            with mock.patch("sdh.main.EmailSender") as email:
                email.send.return_value = constants.E_SUCCESS
                with mock.patch("sdh.main.load_config") as load_config:
                    load_config.return_value = {
                        "email": {"test": "test"},
                        "smart": {},
                    }
                    exit_code = main.main(["--quiet", "--email", "sda"])
                    email.send.assert_not_called()
                    assert exit_code == constants.E_SUCCESS

    def test_main_email_quiet_warn(self):
        with mock.patch("sdh.main.load_config") as load_config:
            load_config.return_value = {
                "email": {"test": "test"},
                "smart": {},
            }
            with mock.patch("sdh.main.SMART") as SMART:
                smart = mock.MagicMock()
                smart.check_disks.return_value = constants.State.WARNING, ["smart"]
                SMART.return_value = smart
                with mock.patch("sdh.main.EmailSender") as email_sender:
                    email = mock.MagicMock()
                    email.send.return_value = constants.E_SUCCESS
                    email_sender.return_value = email
                    exit_code = main.main(["--quiet", "--email", "sda"])
                    load_config.assert_called_once()
                    email.send.assert_called_once()
                    assert exit_code == constants.E_SUCCESS

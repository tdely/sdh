# -*- coding: utf-8 -*-
from smtplib import SMTPException
from unittest import mock

import pytest
from schema import SchemaError

from sdh import exceptions
from sdh.email import EmailSender


class TestEmailSender(object):
    def test_schema_mandatory_settings_ok(self):
        sender = EmailSender(
            {
                "smtp": {
                    "server": "localhost",
                    "username": "usr",
                    "password": "pwd",
                },
                "sender": "asd@asd.com",
                "recipient": "qwe@qwe.com",
            }
        )
        assert sender

    def test_schema_all_settings_ok(self):
        sender = EmailSender(
            {
                "smtp": {
                    "server": "localhost",
                    "port": 123,
                    "username": "usr",
                    "password": "pwd",
                },
                "sender": "asd@asd.com",
                "recipient": "qwe@qwe.com",
            }
        )
        assert sender

    def test_schema_empty_error(self):
        with pytest.raises(SchemaError):
            EmailSender({})

    def test_schema_smtp_port_error(self):
        with pytest.raises(SchemaError) as e:
            EmailSender(
                {
                    "smtp": {
                        "server": "localhost",
                        "port": "port",
                        "username": "usr",
                        "password": "pwd",
                    },
                    "sender": "asd@asd.com",
                    "recipient": "qwe@qwe.com",
                }
            )
        assert str(e.value) == "smtp.port must be integer"

    def test_send_ok(self):
        with mock.patch("sdh.email.ssl"):
            with mock.patch("sdh.email.smtplib.SMTP_SSL") as SMTP_SSL:
                server = mock.MagicMock()
                SMTP_SSL.return_value.__enter__.return_value = server
                sender = EmailSender(
                    {
                        "smtp": {
                            "server": "localhost",
                            "username": "usr",
                            "password": "pwd",
                        },
                        "sender": "asd@asd.com",
                        "recipient": "qwe@qwe.com",
                    }
                )
                sender.send("test", "tset")
        SMTP_SSL.assert_called_once_with("localhost", 465, context=mock.ANY, timeout=5)
        server.login.assert_called_once_with("usr", "pwd")
        server.sendmail.assert_called_once_with(
            "asd@asd.com",
            "qwe@qwe.com",
            "Subject: test\n\ntset",
        )

    def test_send_login_failure(self):
        with mock.patch("sdh.email.ssl"):
            with mock.patch("sdh.email.smtplib.SMTP_SSL") as SMTP_SSL:
                server = mock.MagicMock()
                server.login.side_effect = SMTPException("test")
                SMTP_SSL.return_value.__enter__.return_value = server
                sender = EmailSender(
                    {
                        "smtp": {
                            "server": "localhost",
                            "username": "usr",
                            "password": "pwd",
                        },
                        "sender": "asd@asd.com",
                        "recipient": "qwe@qwe.com",
                    }
                )
                with pytest.raises(exceptions.UserError):
                    sender.send("test", "tset")

    def test_send_smtp_ssl_failure(self):
        with mock.patch("sdh.email.ssl"):
            with mock.patch("sdh.email.smtplib.SMTP_SSL") as SMTP_SSL:
                SMTP_SSL.side_effect = OSError("test")
                SMTP_SSL.return_value.__enter__.side_effect = OSError("test")
                sender = EmailSender(
                    {
                        "smtp": {
                            "server": "localhost",
                            "username": "usr",
                            "password": "pwd",
                        },
                        "sender": "asd@asd.com",
                        "recipient": "qwe@qwe.com",
                    }
                )
                with pytest.raises(exceptions.UserError):
                    sender.send("test", "tset")

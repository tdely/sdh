# -*- coding: utf-8 -*-
from sdh import exceptions
from sdh.constants import E_USER_ERR


class TestExceptions(object):
    def test_user_error(self):
        e = exceptions.UserError("string")
        assert str(e) == "string"
        assert e.code == E_USER_ERR

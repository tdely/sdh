# -*- coding: utf-8 -*-
import re
from unittest import mock

import pytest

from sdh.tools import zfs

STATUS_NORMAL = """  pool: test1
 state: ONLINE
  scan: scrub repaired 0B in 0 days 02:10:41 with 0 errors on Sun Oct 11 02:34:43 2020
config:

	NAME        STATE     READ WRITE CKSUM
	test1       ONLINE       0     0     0
	  mirror-0  ONLINE       0     0     0
	    sdb     ONLINE       0     0     0
	    sdc     ONLINE       0     0     0

errors: No known data errors
"""  # noqa: W191, E101

STATUS_OFFLINE = """  pool: test1
 state: DEGRADED
status: One or more devices has been taken offline by the administrator.
      Sufficient replicas exist for the pool to continue functioning in a
      degraded state.
action: Online the device using 'zpool online' or replace the device with
      'zpool replace'.
  scan: scrub repaired 0B in 0 days 02:10:41 with 0 errors on Sun Oct 11 02:34:43 2020
config:

	NAME        STATE     READ WRITE CKSUM
	test1       DEGRADED     0     0     0
	  mirror-0  DEGRADED     0     0     0
	    sdb     ONLINE       0     0     0
	    sdc     OFFLINE      0     0     0

errors: No known data errors
"""  # noqa: W191, E101

STATUS_MULTIPLE_VDEVS = """  pool: test1
 state: ONLINE
  scan: none requested
config:

	NAME        STATE     READ WRITE CKSUM
	test1       ONLINE       0     0     0
	  mirror-0  ONLINE       0     0     0
	    sdb     ONLINE       0     0     0
	    sdc     ONLINE       0     0     0
	  mirror-1  ONLINE       0     0     0
	    sdd     ONLINE       0     0     0
	    sde     ONLINE       0     0     0

errors: No known data errors
"""  # noqa: W191, E101

STATUS_REMOVE = """  pool: test1
 state: ONLINE
  scan: none requested
remove: Removal of vdev 1 copied 8.50K in 0h0m, completed on Tue Oct 13 22:07:42 2020
    96 memory used for removed device mappings
config:

	NAME          STATE     READ WRITE CKSUM
	test1         ONLINE       0     0     0
	  mirror-0    ONLINE       0     0     0
	    sdb       ONLINE       0     0     0
	    sdc       ONLINE       0     0     0

errors: No known data errors"""  # noqa: W191, E101

STATUS_SPARES = """  pool: test1
 state: ONLINE
  scan: none requested
config:

	NAME          STATE     READ WRITE CKSUM
	test1         ONLINE       0     0     0
	  mirror-0    ONLINE       0     0     0
	    sdb       ONLINE       0     0     0
	    sdc       ONLINE       0     0     0
	spares
	  sdd         AVAIL
	  sde         AVAIL

errors: No known data errors
"""  # noqa: W191, E101

STATUS_ERRORS = """  pool: test1
 state: ONLINE
status: One or more devices has experienced an error resulting in data
    corruption.  Applications may be affected.
action: Restore the file in question if possible.  Otherwise restore the
    entire pool from backup.
   see: http://zfsonlinux.org/msg/ZFS-8000-8A
  scan: none requested
config:

	NAME          STATE     READ WRITE CKSUM
	test1         ONLINE       0     0     0
	  mirror-0    ONLINE       0     0     0
	    sdb       ONLINE       0     0     0
	    sdc       ONLINE       0     0     0

errors: Permanent errors have been detected in the following files:

	<metadata>:<0x0>
	<metadata>:<0x39>
"""  # noqa: W191, E101


@pytest.fixture()
def popen(request):
    process_mock = mock.Mock()
    process_mock.communicate.return_value = (bytes(request.param, "utf8"), bytes())
    process_mock.returncode = 0
    with mock.patch("sdh.tools.zfs.which") as which:
        which.return_value = "/usr/sbin/zpool"
        with mock.patch("sdh.tools.zfs.Popen") as popen:
            popen.return_value = process_mock
            yield


class TestToolsZFS(object):
    attr_mandatory = ["name", "state", "scan", "config", "errors"]
    attr_all = ["status", "action", "remove", "see"] + attr_mandatory

    def test_zpool_missing(self):
        with mock.patch("sdh.tools.zfs.which") as which:
            which.return_value = None
            with pytest.raises(RuntimeError):
                zfs.Pool("test1")

    def test_zpool_nonzero_exit(self):
        err_msg = "error"
        process_mock = mock.Mock()
        process_mock.communicate.return_value = (bytes(), bytes(err_msg, "utf8"))
        process_mock.returncode = 1
        with mock.patch("sdh.tools.zfs.which") as which:
            which.return_value = "/usr/sbin/zpool"
            with mock.patch("sdh.tools.zfs.Popen") as popen:
                popen.return_value = process_mock
                with pytest.raises(RuntimeError) as e:
                    zfs.Pool("test1")
                assert err_msg == str(e.value)

    def test_zpool_parse_failure(self):
        process_mock = mock.Mock()
        process_mock.communicate.return_value = (bytes("trash", "utf8"), bytes())
        process_mock.returncode = 0
        with mock.patch("sdh.tools.zfs.which") as which:
            which.return_value = "/usr/sbin/zpool"
            with mock.patch("sdh.tools.zfs.Popen") as popen:
                popen.return_value = process_mock
                with pytest.raises(RuntimeError) as e:
                    zfs.Pool("test1")
                assert str(e.value).startswith("failed to parse")

    @pytest.mark.parametrize("popen", [STATUS_NORMAL], indirect=["popen"])
    def test_attr_exists(self, popen):
        pool = zfs.Pool("test1")
        for attr in self.attr_all:
            assert hasattr(pool, attr)

    @pytest.mark.parametrize("popen", [STATUS_NORMAL], indirect=["popen"])
    def test_mandatory_attr_set(self, popen):
        pool = zfs.Pool("test1")
        for attr in self.attr_mandatory:
            assert getattr(pool, attr)
        assert pool.name == "test1"
        assert pool.state == "ONLINE"
        assert len(pool.errors) == 1

    @pytest.mark.parametrize("popen", [STATUS_NORMAL], indirect=["popen"])
    def test_conditional_attr_empty(self, popen):
        pool = zfs.Pool("test1")
        assert not pool.status
        assert not pool.remove
        assert not pool.action
        assert not pool.see

    @pytest.mark.parametrize("popen", [STATUS_OFFLINE], indirect=["popen"])
    def test_state_value_degraded(self, popen):
        pool = zfs.Pool("test1")
        assert pool.state == "DEGRADED"
        assert len(pool.errors) == 1

    @pytest.mark.parametrize("popen", [STATUS_OFFLINE], indirect=["popen"])
    def test_status_set(self, popen):
        pool = zfs.Pool("test1")
        assert pool.status
        assert not re.search(r"\s{2,}", pool.status)

    @pytest.mark.parametrize("popen", [STATUS_OFFLINE], indirect=["popen"])
    def test_action_set(self, popen):
        pool = zfs.Pool("test1")
        assert pool.action
        assert not re.search(r"\s{2,}", pool.action)

    @pytest.mark.parametrize("popen", [STATUS_REMOVE], indirect=["popen"])
    def test_remove_set(self, popen):
        pool = zfs.Pool("test1")
        assert pool.remove
        assert not re.search(r"\s{2,}", pool.remove)

    @pytest.mark.parametrize("popen", [STATUS_ERRORS], indirect=["popen"])
    def test_errors_found(self, popen):
        pool = zfs.Pool("test1")
        assert pool.errors
        assert len(pool.errors) == 3
        for line in pool.errors:
            assert not re.search(r"\s{2,}", line)

    @pytest.mark.parametrize("popen", [STATUS_NORMAL], indirect=["popen"])
    def test_str(self, popen):
        pool = zfs.Pool("test1")
        assert "pool: test1" in str(pool)
        assert "state: ONLINE" in str(pool)

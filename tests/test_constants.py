# -*- coding: utf-8 -*-
from sdh import constants
from sdh.constants import State


class TestState(object):
    def test_state_ok(self):
        assert int(State.OK) == constants.E_SUCCESS

    def test_state_warning(self):
        assert int(State.WARNING) == constants.E_SDH_WARN

    def test_state_error(self):
        assert int(State.ERROR) == constants.E_SDH_ERROR

    def test_state_critical(self):
        assert int(State.CRITICAL) == constants.E_SDH_CRIT

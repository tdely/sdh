# -*- coding: utf-8 -*-
"""
sdh.exceptions
~~~~~~~~~~~~~~

Exceptions module.
"""
from sdh import constants


class SDHException(Exception):
    """Base sdh exception."""

    def __init__(self, message: str, code: int):
        self._message = message
        self._code = code
        super(SDHException, self).__init__(message)

    @property
    def code(self) -> int:
        """Exit code assigned to the exception."""
        return self._code


class UserError(SDHException):
    """User error."""

    def __init__(self, message: str):
        super(UserError, self).__init__(message, constants.E_USER_ERR)

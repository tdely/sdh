# -*- coding: utf-8 -*-
"""
sdh.email
~~~~~~~~~

This module defines the Email class, used for sending emails.
"""
import smtplib
import ssl

import schema

from sdh.constants import E_SUCCESS
from sdh.exceptions import UserError


class EmailSender:
    """Facilitates the sending of emails."""

    _schema = schema.Schema(
        {
            "smtp": schema.Schema(
                {
                    "server": schema.Schema(str, error="smtp.server must be string"),
                    schema.Optional("port", default=465): schema.Use(
                        int, error="smtp.port must be integer"
                    ),
                    schema.Optional("timeout", default=5): schema.Use(
                        int, error="smtp.timeout must be integer"
                    ),
                    "username": schema.Schema(
                        str, error="smtp.username must be string"
                    ),
                    "password": schema.Schema(
                        str, error="smtp.password must be string"
                    ),
                }
            ),
            "sender": schema.Schema(str, error="sender must be string"),
            "recipient": schema.Schema(str, error="recipient must be string"),
        }
    )

    def __init__(self, cfg: dict) -> None:
        """Create EmailSender object.

        :param cfg: Email configuration.
        """
        self._cfg = self._schema.validate(cfg)

    def send(self, subject: str, message: str) -> int:
        """Send email.

        :param subject: Email subject.
        :param message: Email message.
        :returns: Exit code.
        :raises: UserError on SMTP failure.
        """
        context = ssl.create_default_context()
        msg = f"Subject: {subject}"
        msg = f"{msg}\n\n{message}"
        try:
            with smtplib.SMTP_SSL(
                self._cfg["smtp"]["server"],
                self._cfg["smtp"]["port"],
                context=context,
                timeout=self._cfg["smtp"]["timeout"],
            ) as server:
                server.login(
                    self._cfg["smtp"]["username"], self._cfg["smtp"]["password"]
                )
                server.sendmail(self._cfg["sender"], self._cfg["recipient"], msg)
                return E_SUCCESS
        except (smtplib.SMTPException, OSError) as e:
            raise UserError(f"Could not send email: {e}")

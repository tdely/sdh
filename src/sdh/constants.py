# -*- coding: utf-8 -*-
"""
sdh.constants
~~~~~~~~~~~~~

Constants used internally by sdh.
"""
from enum import Enum

# Exit codes
E_SUCCESS = 0
E_USER_ERR = 1
E_SDH_WARN = 2
E_SDH_ERROR = 3
E_SDH_CRIT = 4


class State(Enum):
    """Evaluated state."""

    def __int__(self) -> int:
        return int(self.value)

    OK = E_SUCCESS
    """No problems detected."""
    WARNING = E_SDH_WARN
    """Possible issues detected that require review."""
    ERROR = E_SDH_ERROR
    """Problems detected that degrade functionality, but do not cause
    wide-spread unavailability and are not considered immediately damaging."""
    CRITICAL = E_SDH_CRIT
    """Problems detected that cause wide-spread unavailability and/or are
    considered immediately damaging."""

# -*- coding: utf-8 -*-
"""Simple Disk Health.

usage:
  sdh [-heqc <path>] <dev> ... [zfs <zpool> ...]

options:
  -c --cfg=path  Configuration file.
  -h --help      Show help.
  -e --email     Send email, exit code won't show test results.
  -q --quiet     Only print or email on non-OK results.

"""
import os
import sys
from datetime import datetime
from typing import List

import schema
from docopt import DocoptExit, docopt
from ruamel.yaml import YAML

from sdh.constants import E_USER_ERR, State
from sdh.email import EmailSender
from sdh.exceptions import SDHException, UserError
from sdh.smart import SMART
from sdh.zfs import ZFS


def load_args(argv: list = None) -> dict:
    """Load arguments.

    :param argv: Arguments for testing, normal operation reads from environment.
    :returns: Parsed arguments.
    :raises: UserError on invalid arguments.
    """
    args = docopt(__doc__, argv=argv)
    args_schema = schema.Schema(
        {
            schema.Optional("--cfg"): schema.Or(
                None,
                lambda f: os.access(f, os.R_OK),
                error="--cfg must be a readable file",
            ),
            str: object,
        }
    )
    try:
        args = args_schema.validate(args)
        if "zfs" in args["<dev>"]:
            args["zfs"] = True
            i = args["<dev>"].index("zfs")
            args["<zpool>"] = args["<dev>"][i + 1 :]
            args["<dev>"] = args["<dev>"][:i]
            if not args["<zpool>"]:
                raise DocoptExit()
    except schema.SchemaError as e:
        raise UserError(str(e))
    return args


def load_config(path: str) -> dict:
    """Load configuration file.

    :param path: Configuration file path.
    :returns: Loaded configuration.
    :raises: UserError on invalid configuration.
    """
    try:
        yaml = YAML(typ="safe")
        yaml.default_flow_style = False
        cfg = None
        if path:
            with open(path, "r") as f:
                cfg = yaml.load(f)
        cfg_schema = schema.Schema(
            {
                schema.Optional("email"): schema.Schema(
                    dict, error="email must be key/value"
                ),
                schema.Optional("smart", default={}): schema.Schema(
                    dict, error="smart must be key/value"
                ),
                schema.Optional(object): object,
            }
        )
        return cfg_schema.validate(cfg if cfg else {})
    except schema.SchemaError as e:
        raise UserError(str(e))


def main(argv: list = None) -> int:
    """Run sdh.

    :param argv: Arguments for testing, normal operation reads from environment.
    :returns: Exit code.
    """
    try:
        args = load_args(argv)
        cfg = load_config(args.get("--cfg", ""))

        if args["--email"] and not cfg.get("email"):
            print("Missing key: email", file=sys.stderr)
            return E_USER_ERR

        output: List[str] = [str(datetime.now())]
        smart = SMART(cfg["smart"])
        state, smart_output = smart.check_disks(args["<dev>"])
        output.extend(smart_output)
        if args["zfs"]:
            zfs = ZFS(cfg.get("zfs", {}))
            tmp_state, zfs_output = zfs.check_pools(args["<zpool>"])
            output.extend(zfs_output)
            if tmp_state.value > state.value:
                state = tmp_state

        if not args["--quiet"] or state.value > State.OK.value:
            if args["--email"]:
                email = EmailSender(cfg["email"])
                return email.send(
                    f"{os.uname().nodename} disk status is {state.name}",
                    "\n\n".join(output),
                )
            print("\n\n".join(output))
        return int(state)
    except SDHException as e:
        print(e, file=sys.stderr)
        return e.code

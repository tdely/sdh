# -*- coding: utf-8 -*-
"""
sdh.zfs
~~~~~~~

This module defines the ZFS class, used for reviewing the health of ZFS pools.
"""
from typing import List, Tuple

import schema

from sdh.constants import State
from sdh.exceptions import UserError
from sdh.tools.zfs import Pool


class ZFS:
    """The ZFS class is used for condensing `zpool status` results into a State
    enum and output string."""

    _schema = schema.Schema(schema.Optional(object))

    def __init__(self, cfg: dict) -> None:
        """Create ZFS object.

        :param cfg: ZFS configuration.
        """
        self._cfg = self._schema.validate(cfg)

    def check_pools(self, pools: List[str]) -> Tuple[State, List[str]]:
        """Check the ZFS-reported state of pools and evaluate an application
        State.

        :param pools: List of pools to check.
        :returns: Worst evaluated State and summaries.
        """
        state = State.OK
        output: List[str] = []
        for pool in pools:
            t_state, pool_output = self.check_pool(pool)
            output.append(pool_output)
            if t_state.value > state.value:
                state = t_state
        return state, output

    def check_pool(self, name: str) -> Tuple[State, str]:
        """Check the ZFS-reported state of a pool and evaluate an application
        State.

        :param name: Pool to check.
        :returns: Evaluated State and summary.
        :raises: UserError on Pool initialization failure.
        """
        try:
            pool = Pool(name)
        except RuntimeError as e:
            raise UserError(str(e))
        state = State.OK
        if len(pool.errors) > 1:
            state = State.WARNING
        if pool.state == "DEGRADED":
            state = State.ERROR
        elif pool.state == "FAULTED" or pool.state == "UNAVAIL":
            state = State.CRITICAL
        output = self._format_output(pool)
        return state, output

    @staticmethod
    def _format_output(pool: Pool) -> str:
        """Format the Pool properties into output.

        :param pool: Pool object.
        :returns: Formatted output.
        """
        output = [
            f"ZFS pool {pool.name}",
            f"  State:  {pool.state}",
        ]
        if pool.status:
            output.append(f"  Status: {pool.status}")
        if pool.action:
            output.append(f"  Action: {pool.action}")
        if pool.see:
            output.append(f"  See:    {pool.see}")
        output.append(f"  Scan:   {pool.scan}")
        output.append(f"  Config:\n{pool.config}")
        for i, line in enumerate(pool.errors):
            if i == 0:
                output.append(f"  Errors: {line}")
            else:
                output.append(f"    {line}")
        return "\n".join(output)

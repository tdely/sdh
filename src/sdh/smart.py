# -*- coding: utf-8 -*-
"""
sdh.smart
~~~~~~~~~

This module defines the SMART class, used for reviewing the health of physical
disks through SMART.
"""
import warnings
from shutil import which
from typing import List, Tuple

import schema
from pySMART import Device

from sdh.constants import State
from sdh.exceptions import UserError


class SMART:
    """The SMART class is used for translating the SMART temperature and health
    of disks into a State enum and output string."""

    _default = {
        "error_on_warn": False,
        "warn_low": 10,
        "warn_high": 50,
        "critical_low": 0,
        "critical_high": 70,
    }
    _schema = schema.Schema(
        {
            schema.Optional("temperature", default=_default): schema.Schema(
                {
                    schema.Optional(
                        "error_on_warn", default=_default["error_on_warn"]
                    ): schema.Schema(
                        bool, error="temperature.error_on_warn must be bool"
                    ),
                    schema.Optional(
                        "warn_low", default=_default["warn_low"]
                    ): schema.Use(int, error="temperature.warn_low must be integer"),
                    schema.Optional(
                        "warn_high", default=_default["warn_high"]
                    ): schema.Use(int, error="temperature.warn_high must be integer"),
                    schema.Optional(
                        "critical_low", default=_default["critical_low"]
                    ): schema.Use(
                        int, error="temperature.critical_low must be integer"
                    ),
                    schema.Optional(
                        "critical_high", default=_default["critical_high"]
                    ): schema.Use(
                        int, error="temperature.critical_high must be integer"
                    ),
                },
            ),
        }
    )

    def __init__(self, cfg: dict) -> None:
        """Create SMART object.

        :param cfg: SMART configuration.
        :raises: UserError if smartctl is missing.
        """
        if not which("smartctl"):
            raise UserError("smartctl command could not be found")
        self._cfg = self._schema.validate(cfg)

    def check_disks(self, devices: List[str]) -> Tuple[State, List[str]]:
        """Check disks' reported health and compare their temperature against
        set thresholds.

        :param devices: List of disk names to check.
        :returns: Worst evaluated State and summaries.
        """
        state = State.OK
        output: List[str] = []
        for dev in devices:
            t_state, drive_output = self.check_disk(dev)
            output.append(drive_output)
            if t_state.value > state.value:
                state = t_state
        return state, output

    def check_disk(self, name: str) -> Tuple[State, str]:
        """Check a disk's reported health and compare its temperature against
        set thresholds.

        :param name: Name of disk to check.
        :returns: Evaluated State and summary.
        :raises: UserError if disk device missing.
        """
        path = f"/dev/{name}"
        with warnings.catch_warnings():
            warnings.filterwarnings("error")
            try:
                dev = Device(path)
            except Warning:
                raise UserError(f"Device '{path}' does not exist!")
        state, level = self._temp_check(dev)

        drive_output = [
            f"Drive {dev.name} ({dev.model}) serial {dev.serial}",
            f"  Temperature is {level}:       {dev.temperature}",
            f"  SMART health self-assessment: {dev.assessment}",
        ]
        if dev.assessment != "PASS":
            state = State.ERROR
        if dev.messages:
            drive_output.append("  SMART warnings and/or errors:")
            for msg in dev.messages:
                drive_output.append(f"    {msg}")
        return state, "\n".join(drive_output)

    def _temp_check(self, dev: Device) -> Tuple[State, str]:
        """Evaluate disk temperature.

        :param dev: Device object.
        :returns: Evaluated State and level.
        """
        state, level = State.OK, "OK"
        if dev.temperature is None:
            return state, "unknown"
        if dev.temperature >= self._cfg["temperature"]["warn_high"]:
            level = "high"
            if dev.temperature >= self._cfg["temperature"]["critical_high"]:
                state = State.CRITICAL
            else:
                state = State.WARNING
        elif dev.temperature <= self._cfg["temperature"]["warn_low"]:
            level = "low"
            if dev.temperature <= self._cfg["temperature"]["critical_low"]:
                state = State.CRITICAL
            else:
                state = State.WARNING
        return state, level

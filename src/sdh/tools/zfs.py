"""
sdh.tools.zfs
~~~~~~~~~~~~~

This module defines the `Pool` class, used to represent the status information
of a ZFS pool.
Once initialized, class properties contain all relevant information from the
`zpool status` command.
"""
import re
from shutil import which
from subprocess import PIPE, Popen
from typing import List, Optional

R_ZPOOL_STATUS = (
    r"(\s*?pool: (?P<pool>[A-z0-9_]+)\n)"
    r"(\s*state: (?P<state>[A-Z]+)\n)"
    r"(\s*status: (?P<status>.+?)\n)?"
    r"(\s*action: (?P<action>.+?)\n)?"
    r"(\s*see: (?P<see>.+?)\n)?"
    r"(\s*scan: (?P<scan>.+?)\n)"
    r"(\s*remove: (?P<remove>.+?)\n)?"
    r"(\s*config:\n\n(?P<config>.+?)\n\n)"
    r"(\s*errors: (?P<errors>.+))"
)


class Pool:
    """Represents a ZFS pool through the information available from
    `zpool status`."""

    def __init__(self, name: str) -> None:
        """Create and initialize a new Pool.

        :param name: ZFS pool name.
        :raises: RuntimeError if zpool is missing, zpool output can't be parsed,
          or on non-zero zpool exit code
        """
        self._name: str = name

        zpool = which("zpool")
        if not zpool:
            raise RuntimeError("zpool command could not be found")

        cmd = Popen(
            [zpool, "status", "-v", name],
            stdout=PIPE,
            stderr=PIPE,
        )
        _stdout, _stderr = [i.decode("utf8") for i in cmd.communicate()]
        if cmd.returncode != 0:
            raise RuntimeError(_stdout + _stderr)
        match = re.match(R_ZPOOL_STATUS, _stdout, re.DOTALL)
        if not match:
            raise RuntimeError(f"failed to parse zpool status command:\n{_stdout}")

        self._raw: str = _stdout
        self._state: str = match.group("state")
        self._scan: str = match.group("scan")
        self._see: Optional[str] = match.group("see")

        self._status: Optional[str] = (
            " ".join(match.group("status").split()) if match.group("status") else None
        )
        self._action: Optional[str] = (
            " ".join(match.group("action").split()) if match.group("action") else None
        )
        self._remove: Optional[str] = (
            " ".join(match.group("remove").split()) if match.group("remove") else None
        )

        self._config = match.group("config")

        self._errors: List[str] = []
        self._errors.append(match.group("errors").split("\n")[0])
        self._errors.extend(match.group("errors")[len(self._errors[0]) :].split())

    def __str__(self) -> str:
        return self._raw

    def __repr__(self) -> str:
        return "<ZFS pool {0}, state:{1}>".format(
            self.name,
            self.state,
        )

    @property
    def name(self) -> str:
        """Name of the pool."""
        return self._name

    @property
    def state(self) -> str:
        """Current health of the pool.
        This information refers only to the ability to provide the necessary
        replication level."""
        return self._state

    @property
    def status(self) -> Optional[str]:
        """Description of what is wrong with the pool.
        This field is omitted if no problems are found."""
        return self._status

    @property
    def action(self) -> Optional[str]:
        """Recommended action for repairing the problems.
        This field is omitted if no problems are found."""
        return self._action

    @property
    def see(self) -> Optional[str]:
        """Reference to a knowledge article containing detailed repair information.
        This field is omitted if no problems are found."""
        return self._see

    @property
    def scan(self) -> str:
        """Current status of a scrub or resilver operation, which might include
        the date and time that the last operation was completed, operation in
        progress, or if no operation was requested."""
        return self._scan

    @property
    def remove(self) -> Optional[str]:
        """Current status of a removal operation, which might include
        the date and time that the last operation was completed or operation in
        progress.
        This field is omitted if no removal operation is found."""
        return self._remove

    @property
    def config(self) -> Optional[str]:
        """Current pool configuration."""
        return self._config

    @property
    def errors(self) -> List[str]:
        """Identifies known data errors or the absence of known data errors."""
        return self._errors

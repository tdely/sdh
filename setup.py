#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import io
import os

from setuptools import find_packages, setup

NAME = "sdh"
DESCRIPTION = "A simple tool for performing quick health checks on storage devices."
URL = "https://gitlab.com/tdely/sdh"
EMAIL = "cleverhatcamouflage@gmail.com"
AUTHOR = "Tobias Dély"
REQUIRES_PYTHON = ">=3.6.0"
VERSION = None

REQUIRED = [
    "docopt",
    "pySMART.smartx",
    "ruamel.yaml",
    "schema",
]
REQUIRED_SETUP = ["pytest-runner"]
REQUIRED_TESTS = [
    "pytest",
    "coverage",
    "pytest-cov",
    "pytest-print",
    "tox",
]
REQUIRED_DEV = [
    "black",
    "isort",
    "flake8",
    "mypy",
    "pylint",
    "sphinx",
    "sphinx_autodoc_typehints",
    "sphinxcontrib-confluencebuilder",
    "sphinx_rtd_theme",
] + REQUIRED_TESTS
REQUIRED_EXTRAS = {
    "testing": REQUIRED_TESTS,
    "dev": REQUIRED_DEV,
}

here = os.path.abspath(os.path.dirname(__file__))

# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
try:
    with io.open(os.path.join(here, "README.md"), encoding="utf-8") as f:
        long_description = "\n" + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    with open(os.path.join(here, "src", NAME, "__version__.py")) as f:
        exec(f.read(), about)
else:
    about["__version__"] = VERSION

setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type="text/markdown",
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    entry_points={"console_scripts": [f"{NAME}={NAME}.main:main"]},
    install_requires=REQUIRED,
    setup_requires=REQUIRED_SETUP,
    tests_require=REQUIRED_TESTS,
    extras_require=REQUIRED_EXTRAS,
    test_suite="tests",
    include_package_data=True,
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
    ],
)

Changes
=======

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_ a Changelog,
and this project adheres to `Semantic Versioning`_.

.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html

[0.9.2 - 2020-10-20
--------------------
Added
^^^^^
- New SMTP option "timeout" for setting a maximum timeout on establishing SSL connection.

Changed
^^^^^^^
- Fix configuration not loading correctly.
- Use port 465 by default for SMTP SSL.
- Handle SMTP SSL timeout or connection issue more gracefully.

[0.9.1] - 2020-10-19
--------------------
Changed
^^^^^^^
- Fix --cfg option not being optional.

[0.9.0] - 2020-10-17
--------------------
Added
^^^^^
- Initial semi-stable version.
